#include "recorder.hpp"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <fstream>
#include <exception>
#include <iomanip>
#include <assert.h>
#include <cstdlib>
#include <limits>
#include <sstream>

namespace nir {
	Recorder::Recorder() {
		try {
			_imager.connect(); //Be prepared to catch exceptions if the imager is not physically connected to the computer

			// Set Camera properties
			_imager.set_integration_time(5.0); // Milliseconds
			_imager.set_framerate(180.0); // Hz

			_framesize = _imager.get_band_count() * _imager.get_sample_count();
			assert(_framesize * sizeof(unsigned short) == _imager.get_frame_buffer_size_in_bytes());
			_cubesize = _framesize * FRAME_LEN;

			// Read dark and light cal buffers for calibrating output.
			// Assume it was recorded as a single line with same framesize.
			_cal_buffer_len = _framesize;
			if (read_buffer_from_file(_dark_buffer, _cal_buffer_len, DARK_BUFFER_FILENAME)) {
				_free_dark_buffer = true;
			}
			else {
				throw "Failed to read dark cal file.";
			}
			if (read_buffer_from_file(_light_buffer, _cal_buffer_len, LIGHT_BUFFER_FILENAME)) {
				_free_light_buffer = true;
				// Remove dark from the light cal
				remove_dark_noise(_light_buffer, _cal_buffer_len);
			}
			else {
				throw "Failed to read light cal file.";
			}

			// Build header file since its always the same
			std::stringstream ss;
			ss << "ENVI\n";
			ss << "interleave = bil\n";
			ss << "data type = 12\n";
			ss << "bit depth = 14\n";
			ss << "samples = " << _imager.get_sample_count() << "\n";
			ss << "bands = " << _imager.get_band_count() << "\n";
			ss << "lines = " << FRAME_LEN << "\n";
			ss << "byte order = 0\n";
			ss << "framerate = " << _imager.get_framerate() << "\n";
			ss << "shutter = " << _imager.get_integration_time() << "\n";
			ss << "wavelength = {";
			ss << std::setprecision(6);
			int num_bands = _imager.get_band_count();
			for (int i = 0; i < num_bands - 1; i++)
			{
				ss << _imager.get_wavelength_at_band(i) << ", ";
			}
			ss << _imager.get_wavelength_at_band(num_bands - 1) << "}\n";
			_hdr_file = ss.str();

			_initialized = true;
		}
		catch (std::exception const & e)
		{
			std::cerr << "Failed to connect to NIR camera. Recorder is uninitialized.";
			cleanup();
			_initialized = false;
		}
	}

	bool Recorder::is_initialized() {
		return _initialized;
	}

	void Recorder::kill_threads() {
		_read_from_camera = false;
		if (_listener.joinable()) {
			_listener.join();
		}
		if (_frame_checker.joinable()) {
			_frame_checker.join();
		}
	}

	void Recorder::cleanup() {
		// Kill listener
		kill_threads();

		// Free memory
		if (_free_dark_buffer) {
			delete[] _dark_buffer;
		}
		if (_free_light_buffer) {
			delete[] _light_buffer;
		}

		// Attempt to disconnect
		_imager.disconnect();
	}

	void Recorder::read() {
		try {
			_imager.start();
			while (_read_from_camera.load()) {
				// Record and calibrate data
				size_t curr_nir_index;
				uint64_t curr_nir_ts;
				{
					_cv_bool = false;

					_buffer->adjust_head(_framesize);
					curr_nir_index = _buffer->get_head_index();
					auto buffer_ptr = _buffer->get_head_ptr();
					_imager.get_frame(buffer_ptr);

					/*
					* Store timestamp.
					* The imager actually has a get_last_timestamp function that gives the
					* "timestamp" of the last returned line. But this function returns the
					* number of nanoseconds that have passed since the camera was plugged in.
					* Unfortunately this cannot be correlated to anything correctly unless we
					* knew the EXACT (to the ns) moment the camera was plugged in, or we knew
					* EXACTLY how long it took from the camera recording the line to the
					* "get_frame" function returning.
					*
					* Fortunately, the below will most likely be accurate enough for our
					* purposes.
					*/
					curr_nir_ts = std::chrono::duration_cast<std::chrono::nanoseconds>
						(std::chrono::system_clock::now().time_since_epoch()).count();

					// Calibrate data
					remove_dark_noise(buffer_ptr, _framesize);
					ratio_with_light_value(buffer_ptr, _framesize);
				}

				_timestamps->adjust_head(1);
				*(_timestamps->get_head_ptr()) = curr_nir_ts;

				{
					std::lock_guard<std::mutex> guard(_ts_queue_mutex);
					_ts_queue.push_back(std::make_pair(curr_nir_ts, curr_nir_index));
				}

				_cv_bool = true;
				_vector_data_cv.notify_one();

				if (_frame_waiting.load()) {
					std::unique_lock<std::mutex> lk(_vector_data_mutex);
					_vector_data_cv.wait(lk, [this] { return !_frame_waiting.load(); });
					lk.unlock();
				}
			}
			_imager.stop();
		}
		catch (std::exception const & e)
		{
			std::cerr << "Error reading from NIR camera.";
			_imager.stop(); // Just in case
		}
	}

	void Recorder::check_frames() {
		while (_read_from_camera.load()) {
			// Adjust frame data and save frames if necessary
			std::vector<size_t> indices_to_remove{};
			size_t i = 0;

			std::vector<FrameData> frame_start_data_copy;
			{
				std::lock_guard<std::mutex> guard(_frame_start_mutex);
				frame_start_data_copy = _frame_start_data; // copy
			}

			std::vector<std::pair<uint64_t, size_t>> ts_queue_copy;
			{
				std::lock_guard<std::mutex> guard(_ts_queue_mutex);
				ts_queue_copy = _ts_queue; // copy
				_ts_queue.clear();
			}
			int new_rows = ts_queue_copy.size();

			if (new_rows == 0) {
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
				continue;
			}

			for (auto& frame : frame_start_data_copy) {
				if (frame.counter + new_rows >= FRAME_LEN) {
					_frame_waiting = true;
					std::unique_lock<std::mutex> lk(_vector_data_mutex);
					_vector_data_cv.wait(lk, [this]{ return _cv_bool; });
					frame.out_data = std::move(_buffer->get_data_from(frame.closest_index + (_framesize-1), _cubesize));
					lk.unlock();
					_frame_waiting = false;
					_vector_data_cv.notify_one();

					std::thread worker = std::thread(&Recorder::export_frame, this, frame);
					worker.detach();
					indices_to_remove.push_back(i);
				}
				else if (frame.first) {
					frame.first = false;
					frame.counter = 1;
					frame.closest_ts = ts_queue_copy.front().first; // timestamp
					frame.closest_index = ts_queue_copy.front().second; // index
				}

				if (frame.counter <= 2) { // If >2, last ts was not closer than prev and since time does not go backwards, we can stop this check
											   // Stamps are unsigned so need funky maths. Want to check which timestamp is closer to target.
											   // If most recent stamp is closer than last, start data count over.
					bool found_new_closest = false;
					for (auto j = 0; j < new_rows; j++) {
						const auto pair = ts_queue_copy.at(j);
						const auto ts = pair.first;
						const auto nir_index = pair.second;
						// Old to new
						if ((std::max(ts, frame.target_ts) - std::min(ts, frame.target_ts)) <
							(std::max(frame.target_ts, frame.closest_ts) - std::min(frame.target_ts, frame.closest_ts))) {
							frame.closest_index = nir_index;
							frame.closest_ts = ts;
							frame.counter = new_rows - j;
							found_new_closest = true;
						}
					}
					if (!found_new_closest) {
						frame.counter += new_rows;
					}
				}
				else {
					frame.counter += new_rows;
				}
				i++;
			}

			// Remove frame data for exported frames
			{
				std::lock_guard<std::mutex> guard(_frame_start_mutex);
				auto size_diff = _frame_start_data.size() - frame_start_data_copy.size();
				if (size_diff > 0) {
					frame_start_data_copy.insert(frame_start_data_copy.end(),
						_frame_start_data.end() - size_diff, _frame_start_data.end());
				}
				_frame_start_data = std::move(frame_start_data_copy);
				for (const auto i : indices_to_remove) {
					_frame_start_data.erase(_frame_start_data.begin() + i);
				}
			}
		}
	}

	void Recorder::start_recording() {
		_buffer = std::make_unique<ReverseCircularBuffer<unsigned short>>(_framesize * MAX_BUFFER_LEN);
		_timestamps = std::make_unique<ReverseCircularBuffer<uint64_t>>(MAX_BUFFER_LEN);
		_read_from_camera = true;
		_listener = std::thread(&Recorder::read, this);
		_frame_checker = std::thread(&Recorder::check_frames, this);
	}

	void Recorder::stop_recording() {
		kill_threads();
		_buffer.reset();
		_timestamps.reset();
		_frame_start_data.clear();
	}

	void Recorder::set_frame_for_capture(const AxonMsg& msg, const std::string& upload_dir) {
		uint64_t rgb_ts = (NS * msg.sec) + msg.nsec; // NS is uint64_t, so shouldn't overflow in calc
		uint64_t nir_start_ts = rgb_ts + 
			static_cast<uint64_t>(1e9 * (DISTANCE_BTWN_CAMERAS / BELT_SPEED)); // Don't use NS cuz we want a double (doesn't really matter, but cleaner)

		std::lock_guard<std::mutex> guard(_frame_start_mutex);
		_frame_start_data.emplace_back(nir_start_ts, 0, 0, 0, true, upload_dir);
	}

	bool Recorder::read_buffer_from_file(unsigned short*& buffer, int buffer_len, const std::string& filename) {
		try {
			buffer = new unsigned short[buffer_len];
			std::ifstream cubefile;
			cubefile.open(filename.c_str(), std::ios::in | std::ios::binary);
			cubefile.read((char*)buffer, buffer_len * sizeof(unsigned short));
			cubefile.close();
			return true;
		}
		catch (std::exception const & e)
		{
			std::cerr << "Error reading file: " << e.what() << std::endl;
			return false;
		}
	}

	void Recorder::remove_dark_noise(unsigned short*& frame, int buffer_len) {
		assert(buffer_len == _cal_buffer_len);

		for (size_t i = 0; i < buffer_len; i++) {
			auto val = frame[i] - _dark_buffer[i];
			frame[i] = (val > 0) ? val : 0;
		}
	}

	void Recorder::ratio_with_light_value(unsigned short*& frame, int buffer_len) {
		assert(buffer_len == _cal_buffer_len);

		for (size_t i = 0; i < buffer_len; i++) {
			auto val = frame[i];
			// Ratio all measurements against light reference and scale to between 0 and 65535 (ushort max)
			// This means all saved measurements are percentages of reflectance with 65536 possible values.
			// This is preferred over absolute reflectance since that can vary day to day based on environment changes.
			frame[i] = std::round(((1. * val) / _light_buffer[i]) * USHRT_MAX);
		}
	}

	void Recorder::export_frame(const FrameData& frame) {
		// std::string ts_name = ts_to_iso_string(frame.target_ts); // Axon renames anyways...
		std::string data_filename = "output/" + frame.upload_dir + ".bil";
		std::string header_filename = data_filename + ".hdr";

		// Write header file
		std::ofstream outfile(header_filename.c_str());
		outfile << _hdr_file;
		outfile.close();

		// Write data file
		save_buffer_to_file(frame.out_data.data(), _cubesize, data_filename);
		std::cout << "Successfully saved NIR frame." << std::endl; // DEBUG

		// Transfer files to Axon machine
		std::string remote_dir = "/home/amp-admin/amp_data/recordings/" + frame.upload_dir;
		scp(AXON_IP, data_filename + " " + header_filename, remote_dir);

		// TESTING
		//std::this_thread::sleep_for(std::chrono::milliseconds(250));
	}

	std::string Recorder::ts_to_iso_string(uint64_t ts) {
		std::time_t t = ts / NS;
		char ts_buffer[20];
		strftime(ts_buffer, 20, "%FT%T", gmtime(&t));
		std::string ts_str(ts_buffer);

		unsigned int frac_int = (ts % NS) / 1000;
		return ts_str + "." + std::to_string(frac_int);
	}

	void Recorder::scp(const std::string& host, const std::string& local_name, const std::string& remote_name) {
		std::string scp_cmd = "scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i "+ RSA_KEY_PATH 
			+ " " + local_name + " amp-admin@" + host + ":" + remote_name;
		if (system(scp_cmd.c_str()) == 0) {
			std::cout << "Successfully transfered files.\n";
		}
		else {
			std::cerr << "Failed to transfer files!\n";
		}
	}
};