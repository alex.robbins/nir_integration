#pragma once

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace util {
	enum AxonMsgType {
		START = 0,
		STOP,
		FRAME
	};

	struct AxonMsg {
		unsigned char code;	// 0=start, 1=stop, 2=frame
		unsigned int sec;	// Undefined for start/stop messages
		unsigned int nsec;	// Undefined for start/stop messages
	};

	struct FrameData {
		uint64_t target_ts;
		uint64_t closest_ts;
		size_t closest_index;
		int counter;
		bool first;
		std::string upload_dir;
		std::vector<unsigned short> out_data;

		FrameData(uint64_t tts, uint64_t cts, size_t ci, int c, bool f, const std::string& dir)
			: target_ts(tts)
			, closest_ts(cts)
			, closest_index(ci)
			, counter(c)
			, first(f)
			, upload_dir(dir) {}
	};

	// Used to write a datacube buffer to a file
	inline bool save_buffer_to_file(const unsigned short* buffer, int buffer_len, const std::string& filename) {
		try {
			std::ofstream cubefile;
			cubefile.open(filename.c_str(), std::ios::out | std::ios::binary);
			cubefile.write((const char*)buffer, buffer_len * sizeof(unsigned short));
			cubefile.close();
			return true;
		}
		catch (std::exception const & e)
		{
			std::cerr << "Error saving file: " << e.what() << std::endl;
			return false;
		}
	}
}