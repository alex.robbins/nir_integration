#pragma once

#include "circular_buffer.hpp"

#include <vector>

namespace nir {

	/*
	* Simple circular buffer with only some of the functionality that might
	* be expected of it. There is no push/pop functionality because it will
	* be filled in chunks by passing raw memory references into the Resonon
	* imager class. Then, the caller will manually adjust the 'head'.
	*
	* This buffer will insert items in reverse order (will make things easier
	* depending on the orientation of the NIR camera).
	*/
	template<class T>
	class ReverseCircularBuffer {
	public:
		ReverseCircularBuffer(size_t size) {
			_size = size;
			_data = std::vector<T>(size);
		}

		// Call this BEFORE data is added to adjust N indices backwards.
		// For the use case of NIR, added_data should be a factor of _size.
		void adjust_head(size_t data_to_add) {
			int offset = _head - data_to_add;
			if (offset >= 0) {
				_head = offset;
			}
			else {
				_head = _size + offset;
			}
		}

		size_t get_head_index() {
			return _head;
		}

		T* get_head_ptr() {
			return _data.data() + _head;
		}

		size_t get_size() {
			return _size;
		}

		// Will return the data in order from newest to oldest
		std::vector<T> get_data() {
			std::vector<T> temp(_data.begin() + _head, _data.end());
			temp.insert(temp.end(), _data.begin(), _data.begin() + _head);
			return temp;
		}

		std::vector<T> get_data_from(size_t i, size_t l) {
			int start = static_cast<int>(i - (l - 1));
			if (start < 0) {
				std::vector<T> temp(_data.begin(), _data.begin() + (i + 1));
				temp.insert(temp.begin(), _data.end() + start, _data.end());
				return temp;
			} else {
				std::vector<T> temp(_data.begin() + start, _data.begin() + (i+1));
				return temp;
			}
		}

	private:
		size_t _size;
		std::vector<T> _data;

		// Technically points to the next space after 'head', where 
		//  values will be placed... But I like 'head' better.
		size_t _head{ 0 };
	};
}