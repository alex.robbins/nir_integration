#pragma once

#include <vector>

namespace nir {

	/*
	 * Simple circular buffer with only some of the functionality that might
	 * be expected of it. There is no push/pop functionality because it will
	 * be filled in chunks by passing raw memory references into the Resonon
	 * imager class. Then, the caller will manually adjust the 'head'.
	 */
	template<class T>
	class CircularBuffer {
	public:
		CircularBuffer(size_t size) {
			_size = size;
			_data = std::vector<T>(size);
		}

		// Call this after data is added to adjust N indices forward.
		// For the use case of NIR, added_data should be a factor of _size.
		void adjust_head(size_t added_data) {
			_head = (_head + added_data) % _size;
		}

		size_t get_head_index() {
			return _head;
		}

		T* get_head_ptr() {
			return _data.data() + _head;
		}

		size_t get_size() {
			return _size;
		}

		// Will return the data in order from oldest to newest
		std::vector<T> get_data() {
			std::vector<T> temp(_data.begin() + _head, _data.end());
			temp.insert(temp.end(), _data.begin(), _data.begin() + _head);
			return temp;
		}
	private:
		size_t _size;
		std::vector<T> _data;

		// Technically points to the next space after 'head', where 
		//  values will be placed... But I like 'head' better.
		size_t _head{ 0 };
	};
}