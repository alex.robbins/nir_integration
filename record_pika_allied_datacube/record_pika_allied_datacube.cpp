#undef UNICODE
#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <fstream>
#include <exception>
#include <string>
#include <iomanip>
#include <assert.h>
#include <cstdlib>
#include <limits>
#include <thread>

// TCP imports
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

#include "recorder.hpp"
#include "util.hpp"

#pragma comment (lib, "Ws2_32.lib")

#define MAIN_AXON_MSG_LEN 9
#define DEFAULT_BUFLEN 256
#define DEFAULT_PORT "12345"

using namespace util;

void close_socket(SOCKET& socket) {
	closesocket(socket);
	WSACleanup();
}

bool setup_socket(SOCKET& ListenSocket, SOCKET& ClientSocket) {
	WSADATA wsaData;
	int iResult;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return false;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return false;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return false;
	}

	// Setup the TCP listening socket
	iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		close_socket(ListenSocket);
		return false;
	}

	freeaddrinfo(result);
	return true;
}

bool shutdown_socket(SOCKET& socket) {
	int iResult = shutdown(socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
	}
	close_socket(socket);
	return iResult != SOCKET_ERROR;
}

/**
* Allow interacting with Axon and recording datacubes using a Pika NIR GigE imager (Allied Camera using Vimba API).
*
* TCP logic adapted from https://docs.microsoft.com/en-us/windows/win32/winsock/complete-server-code
*/
int main()
{
	std::unique_ptr<nir::Recorder> recorder;
	std::thread t{
		[&recorder] {
		char c;
		while (std::cin >> c)
			if (c == 'q')
			{
				std::cout << "exit command given on stdin\n";
				WSACleanup();
				
				// Race condition? Never heard of it...
				recorder.reset();

				exit(EXIT_SUCCESS);
			}
		}
	};
	t.detach();

	std::cout << "Starting recorder. Press 'q' then 'Enter' to quit.\n";
	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET ClientSocket = INVALID_SOCKET;

	try {
		while (true) {
			recorder.reset();
			recorder = std::make_unique<nir::Recorder>();
			if (!recorder || !recorder->is_initialized()) {
				std::cerr << "Failed to initialize recorder.";
				exit(EXIT_FAILURE);
			}

			int iResult;

			char recvbuf[DEFAULT_BUFLEN];
			int recvbuflen = DEFAULT_BUFLEN;

			if (!setup_socket(ListenSocket, ClientSocket))
				return 1;

			iResult = listen(ListenSocket, SOMAXCONN);
			if (iResult == SOCKET_ERROR) {
				printf("listen failed with error: %d\n", WSAGetLastError());
				close_socket(ListenSocket);
				return 1;
			}

			// Accept a client socket
			ClientSocket = accept(ListenSocket, NULL, NULL);
			if (ClientSocket == INVALID_SOCKET) {
				printf("accept failed with error: %d\n", WSAGetLastError());
				close_socket(ListenSocket);
				return 1;
			}
			printf("Connected to client.\n");

			// No longer need server socket
			closesocket(ListenSocket);

			// Receive until the peer shuts down the connection
			do {
				iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
				if (iResult > 0) {
					// printf("Bytes received: %d\n", iResult); // DEBUG
					AxonMsg msg;
					msg.code = recvbuf[0];
					msg.sec = 0;
					msg.nsec = 0;
					std::string upload_dir;
					if (msg.code == AxonMsgType::FRAME) { // Only bother decoding timestamp if it's a frame msg
						for (size_t i = 1; i <= 4; i++)
						{
							msg.sec |= ((unsigned char)recvbuf[i] << (32 - (8 * i)));
							msg.nsec |= ((unsigned char)recvbuf[i+4] << (32 - (8 * i)));
						}
						size_t str_len = iResult - MAIN_AXON_MSG_LEN;
						upload_dir = std::string(&recvbuf[MAIN_AXON_MSG_LEN], str_len);
					}
					printf("Code: %d, Timestamp: %X, %X\n", msg.code, msg.sec, msg.nsec); // DEBUG

					switch (msg.code) {
					case AxonMsgType::START:
						recorder->start_recording();
						break;
					case AxonMsgType::STOP:
						recorder->stop_recording();
						break;
					case AxonMsgType::FRAME:
						recorder->set_frame_for_capture(msg, upload_dir);
					}
				}
				else if (iResult == 0) {
					printf("Connection closing...\n");
					recorder->stop_recording();
				}
				else {
					printf("recv failed with error: %d\n", WSAGetLastError());
					close_socket(ClientSocket);
					recorder->stop_recording();
					return 1;
				}

			} while (iResult > 0);

			// Shutdown the connection and cleanup since we're done
			if (!shutdown_socket(ClientSocket))
				return 1;
		}
	}
	catch (std::exception const & e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
		closesocket(ListenSocket); // Might not be initialized but want to be safe
		closesocket(ClientSocket);
		WSACleanup();
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}
