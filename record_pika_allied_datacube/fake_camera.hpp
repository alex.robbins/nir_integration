#pragma once

#include <cstdlib>
#include <chrono>
#include <thread>

// Used for testing
class FakeCamera {
private:
	const double WAVELENGTHS[168] = { 886.674, 891.477, 896.281, 901.087, 905.894, 910.702, 915.512, 920.324, 925.136, 929.951, 934.766, 939.583, 944.401, 949.221, 954.042, 958.865, 963.689, 968.515, 973.341, 978.17, 982.999, 987.83, 992.663, 997.497, 1002.33, 1007.17, 1012.01, 1016.85, 1021.69, 1026.53, 1031.37, 1036.22, 1041.07, 1045.91, 1050.76, 1055.61, 1060.47, 1065.32, 1070.17, 1075.03, 1079.89, 1084.75, 1089.61, 1094.47, 1099.33, 1104.2, 1109.07, 1113.93, 1118.8, 1123.67, 1128.55, 1133.42, 1138.29, 1143.17, 1148.05, 1152.93, 1157.81, 1162.69, 1167.57, 1172.46, 1177.34, 1182.23, 1187.12, 1192.01, 1196.9, 1201.79, 1206.69, 1211.59, 1216.48, 1221.38, 1226.28, 1231.18, 1236.09, 1240.99, 1245.9, 1250.8, 1255.71, 1260.62, 1265.53, 1270.45, 1275.36, 1280.28, 1285.19, 1290.11, 1295.03, 1299.95, 1304.88, 1309.8, 1314.73, 1319.65, 1324.58, 1329.51, 1334.44, 1339.38, 1344.31, 1349.25, 1354.18, 1359.12, 1364.06, 1369, 1373.94, 1378.89, 1383.83, 1388.78, 1393.73, 1398.68, 1403.63, 1408.58, 1413.54, 1418.49, 1423.45, 1428.4, 1433.36, 1438.33, 1443.29, 1448.25, 1453.22, 1458.18, 1463.15, 1468.12, 1473.09, 1478.06, 1483.04, 1488.01, 1492.99, 1497.97, 1502.95, 1507.93, 1512.91, 1517.89, 1522.88, 1527.86, 1532.85, 1537.84, 1542.83, 1547.82, 1552.82, 1557.81, 1562.81, 1567.8, 1572.8, 1577.8, 1582.8, 1587.81, 1592.81, 1597.82, 1602.83, 1607.84, 1612.85, 1617.86, 1622.87, 1627.88, 1632.9, 1637.92, 1642.94, 1647.96, 1652.98, 1658, 1663.03, 1668.05, 1673.08, 1678.11, 1683.14, 1688.17, 1693.2, 1698.24, 1703.27, 1708.31 };
	uint64_t _last_ts{ 0 };
public:
	FakeCamera() {};
	void connect() {};
	void start() {};
	void stop() {};
	void disconnect() {};
	void set_integration_time(float int_time) {};
	void set_framerate(float frame_rate) {};
	double get_framerate() {
		return 180.0;
	};
	double get_integration_time() {
		return 5.0;
	}
	int get_band_count() {
		return 168;
	};
	int get_sample_count() {
		return 320;
	};
	int get_frame_buffer_size_in_bytes() {
		return 168 * 320 * 2;
	};
	double get_wavelength_at_band(const int band) {
		return WAVELENGTHS[band];
	};
	void get_frame(unsigned short* buffer) {
		// std::this_thread::sleep_for(std::chrono::milliseconds(2)); // Add fake delay
		// Fill buffer with fake values
		for (int i = 0; i < 168 * 320; i++) {
			buffer[i] = (unsigned short)(rand() % 16000);
		}

		_last_ts = std::chrono::duration_cast<std::chrono::nanoseconds>
			(std::chrono::system_clock::now().time_since_epoch()).count();
	}

	uint64_t get_last_timestamp() {
		return _last_ts;
	}
};