#pragma once

#include "resonon_imager_allied.h"

#include "circular_buffer.hpp"
#include "reverse_circular_buffer.hpp"
#include "fake_camera.hpp"
#include "util.hpp"

#include <atomic>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

using namespace util;

namespace nir {

	/**
	* Used to record datacubes with a Pika NIR GigE imager (Allied Camera using Vimba API)
	*/
	class Recorder {
	public:
		Recorder();
		~Recorder() {
			cleanup();
		}
		void start_recording();
		void stop_recording();
		void set_frame_for_capture(const AxonMsg& msg, const std::string& upload_dir);
		bool is_initialized();

	private:
		Resonon::PikaAllied _imager; // Uncomment if connected to camera
		// FakeCamera _imager; // Uncomment if not connected to camera
		int _framesize;
		int _cubesize;
		bool _initialized{ false };
		std::string _hdr_file;

		std::atomic<bool> _read_from_camera{ false };
		std::thread _listener;
		std::thread _frame_checker;
		std::mutex _vector_data_mutex;
		std::condition_variable _vector_data_cv;
		bool _cv_bool{ false };
		std::atomic<bool> _frame_waiting{ false };
		std::mutex _frame_start_mutex;
		std::mutex _ts_queue_mutex;
		std::vector<std::pair<uint64_t, size_t>> _ts_queue; // ts, index

		int _cal_buffer_len;
		unsigned short* _dark_buffer;
		bool _free_dark_buffer{ false };
		unsigned short* _light_buffer;
		bool _free_light_buffer{ false };

		/*
		 * Image was upside down (as expected since the NIR camera
		 * builds frames bottom up) so ReverseCircularBuffer is needed.
		 */
		std::unique_ptr<ReverseCircularBuffer<unsigned short>> _buffer;
		std::unique_ptr<ReverseCircularBuffer<uint64_t>> _timestamps;

		// Data to help track which NIR lines to use as the frame for a 
		//  particular Axon FRAME message.
		std::vector<FrameData> _frame_start_data;

		void kill_threads();
		void cleanup();
		void read();
		void check_frames();
		bool read_buffer_from_file(unsigned short*& buffer, int buffer_len, const std::string& filename);
		void remove_dark_noise(unsigned short*& frame, int buffer_len);
		void ratio_with_light_value(unsigned short*& frame, int buffer_len);
		void export_frame(const FrameData& frame);
		std::string ts_to_iso_string(uint64_t ts);
		void scp(const std::string& host, const std::string& local_name, const std::string& remote_name);

		// Constants
		const std::string DARK_BUFFER_FILENAME{ "dark_cal.bil" };
		const std::string LIGHT_BUFFER_FILENAME{ "light_cal.bil" };
		const int FRAME_LEN{ 200 };
		const uint64_t NS{ 1000000000 };

		/*
		 * Can probably be low since the NIR camera is downstream from the RGB camera
		 * so the NIR camera will not have seen the area of the belt captured by the 
		 * RGB camera yet when it receives the FRAME message.
		 *
		 * If the NIR camera was upstram of the RGB camera, this buffer would need to be
		 * much larger (since it would have already seen the area of the belt being
		 * captured by the RGB camera by the time it gets the FRAME message).
		 */
		const int MAX_BUFFER_LEN{ 400 };

		/*
		 * For below:
		 * Ideally these would come from Axon, or be read directly somehow, but to keep
		 * things simple, and because its super easy to change and recompile this code, 
		 * they're gonna be hardcoded here.
		 */
		
		// Meters per second (X fpm / 196.85 = Y m/s)
		// ~ 40.3Hz
		const double BELT_SPEED{ 0.495 };

		// Meters. Distance from bottom of RGB camera's FOV to the line scan (assuming
		//  RGB camera is upstream of the linescan).
		const double DISTANCE_BTWN_CAMERAS{ 0.92 };

		// IP/hostname of Axon machine and rsa key, used for file transfer.
		const std::string AXON_IP{ "10.184.64.82" }; // Ideally would probably pull this from the TCP connection
		const std::string RSA_KEY_PATH{ "./id_rsa" };
	};
}