#include <iostream>
#include <fstream>
#include <exception>
#include <string>
#include <iomanip>
#include <assert.h>
#include <cstdlib>
#include <limits>

#include "resonon_imager_allied.h"
#include "fake_camera.hpp"
#include "util.hpp"





int main()
{
	std::cout << "Starting console application...";
	const int LINE_COUNT = 5; //size of datacube to record
	const std::string filename = "Pika_allied_test.bil"; //location to save datacube
	std::string header_filename; // header filename is the same with '.hdr' appended, see below
	int framesize; //size of a single frame in records (number of elements in array)
	int cubesize; //size of complete datacube in records (number of elements in array)
	unsigned short * buffer; //buffer to hold image data
	char imager_type_buffer[32]; //buffer to hold the imager type string
	bool free_main_buffer = false;

	try
	{
		// Initialize imager.
		//Resonon::PikaAllied imager; // Uncomment if connected to camera
		//FakeCamera imager; // Uncomment if not connected to camera
		//imager.connect(); //Be prepared to catch exceptions if the imager is not physically connected to the computer

		//				  // Set Camera properties
		//imager.set_integration_time(5.0); //milliseconds
		//imager.set_framerate(180.0); //Hz
		//							 /*
		//							 ^^^
		//							 180fps with a 6mm lens mounted 0.55m above the belt means that
		//							 if the belt is moving ~97.44fpm, the output image should not be
		//							 skewed.
		//							 */

		//framesize = imager.get_band_count() * imager.get_sample_count();
		//assert(framesize * sizeof(unsigned short) == imager.get_frame_buffer_size_in_bytes());

		//// Read dark and light cal buffers or do calibration
		//// Assume 320 pixels, 168 bands, 1 line (so we can avoid bil header files)
		//if (read_buffer_from_file(DARK_BUFFER, framesize, DARK_BUFFER_FILENAME)) {
		//	FREE_DARK_BUFFER = true;
		//	DARK_BUFFER_LEN = framesize;
		//}
		//else {
		//	throw "Failed to read dark cal file.";
		//}

		//if (read_buffer_from_file(LIGHT_BUFFER, framesize, LIGHT_BUFFER_FILENAME)) {
		//	FREE_LIGHT_BUFFER = true;
		//	LIGHT_BUFFER_LEN = framesize;
		//}
		//else {
		//	throw "Failed to read light cal file.";
		//}

		// Poll the imager type string
		//imager.get_imager_type(imager_type_buffer, 32);

		//Print out the imager status
		//std::cout << "Imager Status:\n";
		//std::cout << "--------------\n";
		//std::cout << std::setw(18) << "Imager Type: " << imager_type_buffer << "\n";
		//std::cout << std::setw(18) << "Framerate: " << imager.get_framerate();
		//std::cout << " [min: " << imager.get_min_framerate();
		//std::cout << ", max: " << imager.get_max_framerate() << "]" << "\n";
		//std::cout << std::setw(18) << "Integration Time: " << imager.get_integration_time();
		//std::cout << " [min: " << imager.get_min_integration_time();
		//std::cout << ", max: " << imager.get_max_integration_time() << "]" << "\n";
		//std::cout << std::setw(18) << "Bands: " << imager.get_band_count() << "\n";
		//std::cout << std::setw(18) << "Samples: " << imager.get_sample_count() << "\n";

		//allocate memory for datacube
		cubesize = framesize * LINE_COUNT;
		buffer = new unsigned short[cubesize];
		if (buffer == 0)
		{
			std::cerr << "Error: memory could not be allocated for datacube";
			exit(EXIT_FAILURE);
		}
		free_main_buffer = true; //if an exception occurs below make sure we free the just allocated block of memory

								 // record the datacube
		std::cout << "\nRecording Datacube" << std::endl;
		imager.start();

		for (int i = 0; i < LINE_COUNT; i++)
		{
			imager.get_frame(&buffer[i * framesize]);
			std::cout << "Line " << i + 1 << " of " << LINE_COUNT << std::endl;

			// TESTING THESE. BROKENNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
			remove_dark_noise(buffer, i*framesize, framesize);
			ratio_with_light_value(buffer, i*framesize, framesize);
		}
		imager.stop();
		std::cout << "Recording Complete\nWriting Datacube to Disk" << std::endl;

		//write an ENVI compatible header file
		header_filename = filename + ".hdr";
		std::ofstream outfile(header_filename.c_str());
		outfile << "ENVI\n";
		outfile << "interleave = bil\n";
		outfile << "data type = 12\n";
		outfile << "bit depth = 14\n";
		outfile << "samples = " << imager.get_sample_count() << "\n";
		outfile << "bands = " << imager.get_band_count() << "\n";
		outfile << "lines = " << LINE_COUNT << "\n";
		outfile << "framerate = " << imager.get_framerate() << "\n";
		outfile << "shutter = " << imager.get_integration_time() << "\n";
		outfile << "wavelength = {";
		outfile << std::setprecision(6);

		int num_bands = imager.get_band_count();
		for (int i = 0; i < num_bands - 1; i++)
		{
			outfile << imager.get_wavelength_at_band(i) << ", ";
		}
		outfile << imager.get_wavelength_at_band(num_bands - 1) << "}\n";
		outfile.close();

		//write data file
		save_buffer_to_file(buffer, cubesize, filename);
		std::cout << "Done." << std::endl;

		// free allocated resources
		imager.disconnect();
		delete[] buffer;

		if (FREE_DARK_BUFFER) {
			delete[] DARK_BUFFER;
		}
		if (FREE_LIGHT_BUFFER) {
			delete[] LIGHT_BUFFER;
		}
	}
	catch (std::exception const & e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
		if (free_main_buffer)
			delete[] buffer;
		if (FREE_DARK_BUFFER)
			delete[] DARK_BUFFER;
		if (FREE_LIGHT_BUFFER)
			delete[] LIGHT_BUFFER;
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}
