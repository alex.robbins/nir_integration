#include <iostream>
#include <fstream>
#include <exception>
#include <string>
#include <iomanip>
#include <assert.h>
#include <cstdlib>
#include <limits>

#include "resonon_imager_allied.h"
#include "fake_camera.hpp"
#include "util.hpp"

unsigned short* DARK_BUFFER;
bool FREE_DARK_BUFFER = false;
std::string DARK_BUFFER_FILENAME = "../record_pika_allied_datacube/dark_cal.bil";

unsigned short* LIGHT_BUFFER;
bool FREE_LIGHT_BUFFER = false;
std::string LIGHT_BUFFER_FILENAME = "../record_pika_allied_datacube/light_cal.bil";

bool calibrate(Resonon::PikaAllied& imager, int buffer_len, unsigned short*& buffer)
{
	// Average 50 measurements together
	const int num_lines = 50;
	double* temp;
	try {
		buffer = new unsigned short[buffer_len];
		temp = new double[buffer_len];
		std::fill(temp, temp + buffer_len, 0.);
		for (auto i = 0; i < num_lines; i++) {
			imager.get_frame(buffer);
			for (auto j = 0; j < buffer_len; j++) {
				temp[j] += ((1. * buffer[j]) / num_lines);
			}
		}

		for (auto i = 0; i < buffer_len; i++) {
			buffer[i] = static_cast<unsigned short>(std::round(temp[i]));
		}

		delete[] temp;
		return true;
	}
	catch (std::exception const & e) {
		std::cerr << "Error: " << e.what() << std::endl;
		delete[] temp;
		return false;
	}
}

bool calibrate_light(Resonon::PikaAllied& imager, int buffer_len)
{
	FREE_LIGHT_BUFFER = calibrate(imager, buffer_len, LIGHT_BUFFER);
	return util::save_buffer_to_file(LIGHT_BUFFER, buffer_len, LIGHT_BUFFER_FILENAME) && FREE_LIGHT_BUFFER;
}

bool calibrate_dark(Resonon::PikaAllied& imager, int buffer_len)
{
	FREE_DARK_BUFFER = calibrate(imager, buffer_len, DARK_BUFFER);
	return util::save_buffer_to_file(DARK_BUFFER, buffer_len, DARK_BUFFER_FILENAME) && FREE_DARK_BUFFER;
}

// Used for testing when not connected to camera
bool calibrate_dark(FakeCamera& imager, int buffer_len) {
	DARK_BUFFER = new unsigned short[buffer_len];
	for (int i = 0; i < 168 * 320; i++) {
		DARK_BUFFER[i] = (unsigned short)100;
	}
	FREE_DARK_BUFFER = true;
	return util::save_buffer_to_file(DARK_BUFFER, buffer_len, DARK_BUFFER_FILENAME);
};
bool calibrate_light(FakeCamera& imager, int buffer_len) {
	LIGHT_BUFFER = new unsigned short[buffer_len];
	for (int i = 0; i < 168 * 320; i++) {
		LIGHT_BUFFER[i] = (unsigned short)16000;
	}
	FREE_LIGHT_BUFFER = true;
	return util::save_buffer_to_file(LIGHT_BUFFER, buffer_len, LIGHT_BUFFER_FILENAME);
};

/**
* Allow the user to perform dark and/or light calibration. Both calibrations will record a single line of data that will be
* saved to a file for use by the actual integration code.
*
* To perform dark calibration, ensure the belt is not moving and the halogen lamps are off.
* To perform light calibration, ensure the belt is not moving, the halogen lamps are both on and the white reference calibration
* bar is directly underneath the NIR camera's field of view.
*/
int main()
{
	std::cout << "Starting calibration...";
	int framesize; // Size of a single frame in records (number of elements in array)

	try
	{
		// Initialize imager.
		Resonon::PikaAllied imager; // Uncomment if connected to camera
		//FakeCamera imager; // Uncomment if not connected to camera
		imager.connect(); // Be prepared to catch exceptions if the imager is not physically connected to the computer

		// Set Camera properties
		imager.set_integration_time(5.0); // Milliseconds
		imager.set_framerate(180.0); // Hz


		framesize = imager.get_band_count() * imager.get_sample_count();
		assert(framesize * sizeof(unsigned short) == imager.get_frame_buffer_size_in_bytes());

		// Read dark and light cal buffers or do calibration
		// Assume 320 pixels, 168 bands, 1 line (so we can avoid bil header files)
		char choice;
		std::cout << "\nDo you want to perform dark calibration? y/n ";
		std::cin >> choice;
		if (choice == 'y') {
			imager.start();
			calibrate_dark(imager, framesize);
			imager.stop();
		}

		std::cout << "\nDo you want to perform light calibration? y/n ";
		std::cin >> choice;
		if (choice == 'y') {
			imager.start();
			calibrate_light(imager, framesize);
			imager.stop();
		}

		// Free allocated resources
		imager.disconnect();
		if (FREE_DARK_BUFFER) {
			delete[] DARK_BUFFER;
		}
		if (FREE_LIGHT_BUFFER) {
			delete[] LIGHT_BUFFER;
		}
	}
	catch (std::exception const & e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
		if (FREE_DARK_BUFFER)
			delete[] DARK_BUFFER;
		if (FREE_LIGHT_BUFFER)
			delete[] LIGHT_BUFFER;
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}
